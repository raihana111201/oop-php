<?php
	/**
	 * Animal Class Bro
	 */
	class Animal
	{
		public $name;
		public $legs = 2;
		public $cold_blooded = false;

		function __construct($name)
		{
			# code...
			$this->name = $name;
		}

		function getlegs($legs){
			$this->legs = $legs;
		}
		
		function get_cold_blooded($cold_blooded){
			$this->cold_blooded = $cold_blooded;
		}

	}
?>