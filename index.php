<?php
	include 'animal.php';
	include 'Frog.php';
	include 'Ape.php';


	$sheep = new Animal("shaun");
	echo $sheep->name;
	echo "<br>";
	echo $sheep->legs;
	echo $sheep->cold_blooded;

	echo "<br>";

	$sungokong = new Ape("kera sakti");
	$sungokong->yell();

	echo "<br>";

	$kodok = new Frog("buduk");
	$kodok->jump() ;
?>